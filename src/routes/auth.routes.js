const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwtauth = require('../passport/jwt-auth');
//Ejemplo de login banRegio no productivo
router.post('/login', async (req, res, next ) => {
    passport.authenticate('login', {session: false}, (err, user,info) => {
        if(err || !user){
            return res.status(400).json({
                message: info.message
            })
        }else{
            const token = jwtauth.createToken(user.id);
            return res.status(200).json({
                message: ['Exitoso'],
                token: token,
                user: {
                    username: user.username,
                    email: user.email
                }
            });
        }
        
    })(req,res,next);
});
//Ejemplo de creación de usuario BanRegio para pruebas de tecnologia
router.post('/signup', async (req, res, next) => {
    passport.authenticate('signup',{session: false}, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: info.message,            
            });
        }else{
            const token = jwtauth.createToken(user.id);
            return res.status(200).json({
                message: ['Exitoso'],
                token: token
            });
        }
    })(req,res,next)
});

router.post('/logout', (req, res, next) => {
    res.json({
        message: "Cerraste sesion"
    })
});

module.exports = router;
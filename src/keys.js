module.exports = {
    mongodb: {
        URL: 'mongodb://localhost:27017/BanregioAPITest'
    },
    jwtSecret: {
        secret: 'EstoEsUnTestAPIConNombreDeBANREGIOdePrueba'
    }
};